/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bdciisa.dao;

import com.mycompany.bdciisa.dao.exceptions.NonexistentEntityException;
import com.mycompany.bdciisa.dao.exceptions.PreexistingEntityException;
import com.mycompany.bdciisa.entity.ConContacto;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesoto
 */
public class ConContactoJpaController implements Serializable {

    public ConContactoJpaController() {
            }
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ConContacto conContacto) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(conContacto);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findConContacto(conContacto.getRut()) != null) {
                throw new PreexistingEntityException("ConContacto " + conContacto + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ConContacto conContacto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            conContacto = em.merge(conContacto);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = conContacto.getRut();
                if (findConContacto(id) == null) {
                    throw new NonexistentEntityException("The conContacto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ConContacto conContacto;
            try {
                conContacto = em.getReference(ConContacto.class, id);
                conContacto.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The conContacto with id " + id + " no longer exists.", enfe);
            }
            em.remove(conContacto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ConContacto> findConContactoEntities() {
        return findConContactoEntities(true, -1, -1);
    }

    public List<ConContacto> findConContactoEntities(int maxResults, int firstResult) {
        return findConContactoEntities(false, maxResults, firstResult);
    }

    private List<ConContacto> findConContactoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ConContacto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ConContacto findConContacto(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ConContacto.class, id);
        } finally {
            em.close();
        }
    }

    public int getConContactoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ConContacto> rt = cq.from(ConContacto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
