/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.bdciisa.dao.ConContactoJpaController;
//import com.mycompany.bdciisa.dao.ContactoJpaController;
import com.mycompany.bdciisa.dao.exceptions.NonexistentEntityException;
import com.mycompany.bdciisa.entity.ConContacto;
//import com.mycompany.bdciisa.entity.Contacto;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jesoto
 */
@WebServlet(name = "DataController", urlPatterns = {"/DataController"})
public class DataController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DataController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DataController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       System.out.print("doPost");
        
      String accion=request.getParameter("accion");
      if (accion.equals("contac_ingreso")) {
        
        try {
            String rut=request.getParameter("rut");
            String nombre=request.getParameter("nombre");
            String apellidos=request.getParameter("apellidos");
            String email=request.getParameter("email");
            String telefono=request.getParameter("telefono");
            String carrera=request.getParameter("carrera");
            
            //intity 
            ConContacto contacto=new ConContacto();
        //    Contacto contacto=new Contacto();
            contacto.setRut(rut);
            contacto.setNombre(nombre);
            contacto.setApellidos(apellidos);
            contacto.setEmail(email);
            contacto.setTelefono(telefono);
            contacto.setCarrera(carrera);
            
            //dao
          //ContactoJpaController
          //  ContactoJpaController     dao=new ContactoJpaController();
            ConContactoJpaController dao2=new ConContactoJpaController();
            
            
        //    dao.create(contacto);
            dao2.create(contacto);
            request.getRequestDispatcher("index.jsp").forward(request, response);
    
        } catch (Exception ex) {
            Logger.getLogger(DataController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      if (accion.equals("contac_solicitud")) {
      
          // dao 
           ConContactoJpaController dao2=new ConContactoJpaController();
           List<ConContacto> lista=dao2.findConContactoEntities();
           request.setAttribute("listaSolicitudes", lista);
           request.getRequestDispatcher("lista_solicitud.jsp").forward(request, response);                         
      }
     if (accion.equals("contac_eliminar")) {
      
          try {
               String rut=request.getParameter("seleccion");
               // dao
               ConContactoJpaController dao2=new ConContactoJpaController();
               dao2.destroy(rut);
               request.getRequestDispatcher("index.jsp").forward(request, response);
              System.out.print("ELIMINADO");
           } catch (NonexistentEntityException ex) {
               Logger.getLogger(DataController.class.getName()).log(Level.SEVERE, null, ex);
           }
      }  
      if (accion.equals("contac_mod")) {
           String rut=request.getParameter("seleccion");
              // dao 
            ConContactoJpaController dao2=new ConContactoJpaController();
            ConContacto concontacto =dao2.findConContacto(rut);
             request.setAttribute("Solicitudes", concontacto);
             request.getRequestDispatcher("consulta.jsp").forward(request, response);
      }
      
      if (accion.equals("contac_editar")) {
           try {
               //String rut=request.getParameter("seleccion");
               // dao
               
//            ConContacto concontacto =dao2.findConContacto(rut);
//            // request.setAttribute("Solicitudes", concontacto);

               String rut = request.getParameter("rut");
               String nombre = request.getParameter("nombre");
               String apellidos = request.getParameter("apellidos");
               String email = request.getParameter("email");
               String telefono = request.getParameter("telefono");
               String carrera = request.getParameter("carrera");
               ConContacto concontacto = new ConContacto();
               concontacto.setRut(rut);
               concontacto.setNombre(nombre);
               concontacto.setApellidos(apellidos);
               concontacto.setEmail(email);
               concontacto.setTelefono(telefono);
               concontacto.setCarrera(carrera);

               ConContactoJpaController dao2 = new ConContactoJpaController();
               dao2.edit(concontacto);


            //   dao2.edit(concontacto);
            request.getRequestDispatcher("index.jsp").forward(request, response);

                System.out.print("Modificado");
            } catch (Exception ex) {
                Logger.getLogger(DataController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         processRequest(request, response);
      }
     

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
