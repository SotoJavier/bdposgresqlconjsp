<%-- 
    Document   : lista_solicitud
    Created on : 25-04-2021, 20:22:05
    Author     : jesoto
--%>

<%@page import="com.mycompany.bdciisa.entity.ConContacto"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.mycompany.bdciisa.dao.ConContactoJpaController"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% 
List<ConContacto> solicitud = (List<ConContacto>) request.getAttribute("listaSolicitudes");
Iterator<ConContacto> itesolicitudes = solicitud.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
          <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    
     <body style="background-color:grey;">
        <h1>LISTADOS DE NUEVOS POSTULANTE</h1>
        <b><h4> * * Revise la lista * *</h4></B>
         <div align="center"> <img src="img/ingreso.png" id="miImagen" width="50" height="50"></div>
    <div align ="center">
        <h2><u>Modifica tus Datos o envia la lista</u></h2>
        <form name="form" action="DataController" method="POST">
    
            <table border="1">
                    <thead>
                    <th>Rut</th>
                    <th>Nombre </th>
                    <th>Apellidos </th>
                    <th>E-Mail </th>
                    <th>Teléfono </th>
                    <th>Carrera </th>
                    <th>Selecciona</th>
             
                    <th> </th>
         
                    </thead>
                    <tbody>
                        <%while (itesolicitudes.hasNext()) {
                       ConContacto cm = itesolicitudes.next();%>
                        <tr>
                            <td><%= cm.getRut()%> </td>
                            <td><%= cm.getNombre()%></td>
                            <td><%= cm.getApellidos()%></td>
                            <td><%= cm.getEmail()%></td>
                            <td><%= cm.getTelefono()%></td>
                            <td><%= cm.getCarrera()%></td>
                          
                         
                 <td> <input type="radio" name="seleccion" value="<%= cm.getRut()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
             <button type="submit" name ="accion" value="contac_mod" class="btn btn-success">Modificar Solicitudes</button>
             <button type="submit" name ="accion" value="contac_eliminar" class="btn btn-success">Eliminar de Solicitud</button>
        </form> 
    </div>   
    </body>
</html>
