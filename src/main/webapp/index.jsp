<%-- 
    Document   : index
    Created on : 24-04-2021, 17:01:40
    Author     : jesoto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body style="background-color:grey;">
        <h1>BIENVENIDO NUEVOS POSTULANTE</h1>
        <b><h4> * * Llena los campos que se detallan * *</h4></B>
    <div align="center"> <img src="img/ingreso.png" id="miImagen" width="50" height="50"></div>
    <div align ="center">
        <h2><u>Ingresa tus Datos y te Contactaremos</u></h2>
        <form name="form" action="DataController" method="POST">
        <p><strong>Rut</strong><br />
            <span class="wpcf7-form-control-wrap rut"><input type="text" name="rut" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="rut" aria-required="true" aria-invalid="false" /></span> </p>
        <p><strong>Nombre</strong><br />
            <span class="wpcf7-form-control-wrap nombre"><input type="text" name="nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="nombre" aria-required="true" aria-invalid="false" /></span> </p>
        <p><strong>Apellidos</strong><br />
            <span class="wpcf7-form-control-wrap apellidos"><input type="text" name="apellidos" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="apellidos" aria-required="true" aria-invalid="false" /></span> </p>
        <p><strong>E-Mail</strong><br />
            <span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="email" aria-required="true" aria-invalid="false" /></span> </p>
        <p><strong>Teléfono</strong><br />
            <span class="wpcf7-form-control-wrap telefono"><input type="tel" name="telefono" value="" size="40" maxlength="12" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" id="telefono" aria-required="true" aria-invalid="false" /></span> </p>
        <p><strong>Carrera</strong><br />
            <span class="wpcf7-form-control-wrap carrera"><input type="text" name="carrera" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="carrera" aria-required="true" aria-invalid="false" /></span> </p>
           <!--  <span class="wpcf7-form-control-wrap programa"><select name="carrera" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" id="programa" aria-required="true" aria-invalid="false"><option value="">---</option><option value="curso1">Ingeniería en Informática</option><option value="curso2">Técnico en Programación y Análisis de Sistema</option></select></span> -->
        </p>
        <button type="submit" name ="accion" value="contac_ingreso" class="btn btn-success">Ingreso de Solicitud</button>
        <button type="submit" name ="accion" value="contac_solicitud" class="btn btn-success">Listar las Solicitudes</button>
</form> 
        </div>       
    </body>
</html>

  
  
  